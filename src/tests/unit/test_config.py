import os


def test_testing_config(setup_app):
    setup_app.config.from_object("src.config.TestingConfig")
    assert setup_app.config["SECRET_KEY"] == "my_precious"
    assert setup_app.config["TESTING"]
    assert not setup_app.config["PRESERVE_CONTEXT_ON_EXCEPTION"]
    assert setup_app.config["SQLALCHEMY_DATABASE_URI"] == os.environ.get(
        "DATABASE_TEST_URL"
    )


def test_development_config(setup_app):
    setup_app.config.from_object("src.config.DevelopmentConfig")
    assert setup_app.config["SQLALCHEMY_DATABASE_URI"] == os.environ.get("DATABASE_URL")
