from flask import json

from src import create_app


def test_get_success(setup_app):
    app = create_app()
    client = app.test_client()
    ret = json.loads(client.get("/ping").data)
    assert ret["status"] == "success"
